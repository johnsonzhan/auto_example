json.extract! user, :id, :title, :start, :created_at, :updated_at
json.url user_url(user, format: :json)
